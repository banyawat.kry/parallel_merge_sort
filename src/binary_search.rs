pub fn search_index<T : Ord + Send + Copy>(xs: &[T], l: usize, r: usize, target: T) -> usize
{
    if r >= l {
        let mid   =  l + (r - l) / 2;

        if xs[mid] == target { return mid + 1}

        if xs[mid] > target {
            if mid as isize - 1  >= 0 {
                return  search_index(xs, l, mid - 1 , target)
            }else{
                return 0
            }

        }else{
            return   search_index(xs, mid + 1, r, target)
        }
    }
   r+1
}