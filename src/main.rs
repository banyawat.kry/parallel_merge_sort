use rayon::join;
use rayon::prelude::*;
use std::fmt::{Display, Debug};
use std::time::{Duration, Instant};
use std::collections::HashMap;

mod binary_search;

fn parallel_merge_sort<T: Ord + Send + Copy + Sync + Send>(xs: &mut [T])
{
    let n = xs.len();
    let mut buf = Vec::with_capacity(n);

    // We always overwrite the buffer before reading to it, and letting rust
    // initialize it would increase the critical path to O(n).
    unsafe {
        buf.set_len(n);
    }
    merge_sort(xs,  &mut buf[..]);
}

fn merge_sort<T: Ord + Send + Copy + Sync + Send>(xs: &mut [T], dst: &mut [T])
{
    let n = xs.len();
    let mid = n/2;

    if n <= 1 {return }

    let (left, right) = xs.split_at_mut(mid);

    {
        // Sort each half.
        let (dst1, dst2) = dst.split_at_mut(mid);

        join(
            || merge_sort(left, dst1),
            || merge_sort( right, dst2)
        );
    }

    merge(left, right, dst);
    xs.copy_from_slice(dst);
}


fn merge<T: Ord + Send + Copy + Sync + Send>(a: &mut [T] , b: &mut [T],  dst: &mut [T])
{

    let mut left:HashMap<usize,  T> = HashMap::new();
    let mut right:HashMap<usize, T> = HashMap::new();
    join(
        || merge_helper(b,a, &mut left),
         || merge_helper(a,b , &mut right)
    );

    dst
        .par_iter_mut()
        .enumerate()
        .for_each(|(idx,d)| {

            let r = right.get(&idx);
            let l = left.get(&idx);

            if let Some(rr) = r{
                *d = *rr
            }
            if let Some(ll) = l{
                *d = *ll
            }

        });
}

//xs for search
//ts for target
fn merge_helper<T:Ord + Send + Copy + Sync + Send>(xs:  &[T], ts: &[T],  dst: &mut HashMap<usize,  T>){
    let tn= ts.len();
    let xn = xs.len();
    for i in 0..tn {
        let r = binary_search::search_index( xs, 0, xn - 1, ts[i]);
        dst.insert(i+r,  ts[i]);
    }
}


fn main()
{

    let mut test: [i32; 20]  = rand::random();
    //let mut test = [6,5,4,3,2,1];
    let mut sort_test = test;
    sort_test.sort();


    let start = Instant::now();
    parallel_merge_sort(&mut test);
    let duration = start.elapsed();

    println!("Time elapsed is: {:?}", duration);
    assert_eq!(test,    sort_test);
    print!("{:?} ", test);
}